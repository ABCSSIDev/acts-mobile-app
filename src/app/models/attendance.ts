export interface Attendance {
    employee_id: String;
    date: String;
    name: String;
    designation: String;
    breaks: String;
    time_in: String;
    image: String;
    location: String;
    longitude: String;
    latitude: String;
    breaks_status: String;
}
