export class User {
    id: number;
    email: string;
    name: string;
    role: string;
    employee: Array<0>;
}