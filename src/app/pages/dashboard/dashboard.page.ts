import { Component, OnInit } from '@angular/core';
import { LoadingController, MenuController } from '@ionic/angular';
import { Attendance } from 'src/app/models/attendance';
import { User } from 'src/app/models/user';
import { AlertService } from 'src/app/services/alert.service';
import { AttendanceService } from 'src/app/services/attendance.service';
import { AuthService } from 'src/app/services/auth.service';
import { CurrentLocationService } from 'src/app/services/current-location.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  user: User;
  today: any;
  attendance: Attendance;

  informations: any;

  constructor(
    private menu: MenuController,
    private authService: AuthService,
    private attendanceService: AttendanceService,
    private loadingCtrl: LoadingController,
    private locationService: CurrentLocationService,
    private alertService: AlertService
  ) { 
    this.menu.enable(true);
    setInterval(() => { 
      this.refreshTime(); // Now the "this" still references the component
   }, 1000);
  }

  ngOnInit() {
    this.informations = "Attendance Management for ACableworks Technology Solutions in one App!.";
    this.today = Date.now();
  }

  refreshTime() {
    this.today = Date.now()
  }

  ionViewWillEnter() {
    this.locationService.askToTurnOnGPS();
    this.authService.user().subscribe(
      user => {
        this.user = user;
        this.getAttendanceInfo(this.user['employee']['employee_no']);
      }
    );
  }

  getAttendanceInfo(employeeno: String) {
    this.attendanceService.getEmployeeAttendance(employeeno).subscribe(
      attendance => {
        this.attendance = attendance
        this.updateInfo();
        console.log(this.attendance);
      }
    );
  }

  async startBreak() {
    this.getAttendanceInfo(this.user['employee']['employee_no']);

    const loading = await this.loadingCtrl.create({message: 'Loading....'});
    loading.present();
    this.attendanceService.employeeStartBreak(this.user['employee']['employee_no'], this.attendance['attendance_id']).subscribe(
      attendance => {
        loading.dismiss();
        this.alertService.presentToast('Successfully Start Break!');
        this.attendance = attendance;
      }
    );
  }

  async timeinBreak() {
    this.getAttendanceInfo(this.user['employee']['employee_no']);

    const loading = await this.loadingCtrl.create({message: 'Loading....'});
    loading.present();
    this.attendanceService.employeeEndBreak(this.user['employee']['employee_no'], this.attendance['attendance_id']).subscribe(
      attendance => {
        loading.dismiss();
        this.alertService.presentToast('Successfully Start Work after Break!');
        this.attendance = attendance;
        // this.informations = attendance['time_in'];
      }
    );
  }

  async timein() {
    const loading = await this.loadingCtrl.create({message: 'Loading....'});
    loading.present();
    this.attendanceService.employeeTimeIn(this.user['employee']['employee_no'], null).subscribe(
      attendance => {
        loading.dismiss();
        this.alertService.presentToast('Successfully Start you Work!');
        this.attendance = attendance;
        this.updateInfo();
      }
    );
  }

  async endShift() {
    this.getAttendanceInfo(this.user['employee']['employee_no']);

    const loading = await this.loadingCtrl.create({message: 'Loading....'});
    loading.present();
    this.attendanceService.employeeTimeOut(this.user['employee']['employee_no'], this.attendance['attendance_id']).subscribe(
      attendance => {
        loading.dismiss()
        this.updateInfo();
        this.alertService.presentToast('Successfully End your Shift!');
        this.attendance = attendance
      }
    );
  }

  updateInfo() {
    this.informations = '';
    if (this.attendance['time_in'] != null) {
      this.informations += this.attendance['time_in'];
    }

    if (this.attendance['location_address'] != null) {
      this.informations += ' | At ' + this.attendance['location_address'];
    }

    if (this.attendance['hours_worked'] != '00:00') {
      this.informations += ' | Hours Worked: ' + this.attendance['hours_worked'];
    }

    if (this.attendance['time_out'] != null) {
      this.informations += ' | End Shift: ' + this.attendance['time_out'];
    }
  }
}
