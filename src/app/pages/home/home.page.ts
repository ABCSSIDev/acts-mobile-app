import { Component, OnInit } from '@angular/core';
import { LoadingController, MenuController } from '@ionic/angular';
import { AttendanceService } from 'src/app/services/attendance.service';
import { AuthService } from 'src/app/services/auth.service';
import { Attendance } from 'src/app/models/attendance';
import { User } from 'src/app/models/user';
import { CurrentLocationService } from 'src/app/services/current-location.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  user: User;
  today: any;
  attendance: Attendance;

  constructor(
    private menu: MenuController,
    private authService: AuthService,
    private attendanceService: AttendanceService,
    private loadingCtrl: LoadingController,
    private locationService: CurrentLocationService
  ) {
    this.menu.enable(true);

    setInterval(() => { 
      this.refreshTime(); // Now the "this" still references the component
   }, 1000);
  }

  ngOnInit() {
    this.today = Date.now();
  }

  refreshTime() {
      this.today = Date.now()
  }

  ionViewWillEnter() {
    this.locationService.askToTurnOnGPS();
    
    this.authService.user().subscribe(
      user => {
        this.user = user;
        this.getAttendanceInfo(this.user['employee']['employee_no']);
      }
    );
  }

  getAttendanceInfo(employeeno: String) {
    this.attendanceService.getEmployeeAttendance(employeeno).subscribe(
      attendance => {
        this.attendance = attendance
        console.log(this.attendance);
      }
    );
  }

  startBreak() {

  }

  timeinBreak() {

  }

  async timein() {
    const loading = await this.loadingCtrl.create({message: 'Loading....'});
    loading.present();
    this.attendanceService.employeeTimeIn(this.user['employee']['employee_no'], null).subscribe(
      attendance => {
        loading.dismiss();
        this.attendance = attendance
      }
    );
  }

  async endShift() {
    // const loading = await this.loadingCtrl.create({message: 'Loading....'});
    // loading.present();
    // this.attendanceService.employeeTimeOut(this.user['employee']['employee_no']).subscribe(
    //   attendance => {
    //     loading.dismiss();
    //     this.attendance = attendance
    //   }
    // );
  }

}
