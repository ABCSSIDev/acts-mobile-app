import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController, NavController } from '@ionic/angular';
import { AlertService } from '../../../services/alert.service';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  form: FormGroup;

  constructor(
    private modalController: ModalController,
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required])
    })
  }

  // Dismiss Login Modal
  dismissLogin() {
    this.modalController.dismiss();
  }

  async submitLogin() {
    const loading = await this.loadingCtrl.create({message: 'Loading....'});
    loading.present();
    this.authService.login(this.form.value).subscribe(
      data => {
        loading.dismiss();
        this.alertService.presentToast("Logged In");
      },
      error => {
        loading.dismiss();
        this.alertService.presentToast('Error:' + error.message);
      },
      () => {
        loading.dismiss();
        this.dismissLogin();
        this.navCtrl.navigateRoot('/dashboard');
      }
    );

  }
}
