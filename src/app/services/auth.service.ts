import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Login } from '../models/login';
import { EnvService } from './env.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { tap } from 'rxjs/operators';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  token:any;
  private userData = new Subject<any>();

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private storage: NativeStorage
  ) { }

  login(login: Login): Observable<Login> {
    return this.http.post<Login>(`${this.env.APP_URL}auth/login`, login).pipe(
      tap(token => {
        this.storage.setItem('token', token)
        .then(
          () => {
            console.log('Token Stored');
          },
          error => console.error('Error storing item', error)
        );
        this.token = token;
        this.isLoggedIn = true;
        return token;
      }),
    );
  }
  
  getObservable(): Subject<any> {
    return this.userData;
  }

  logout() {
    const headers = new HttpHeaders({
      'Authorization': this.token["token_type"]+" "+this.token["token"]
    });
    return this.http.get(`${this.env.APP_URL}auth/logout`, { headers: headers })
    .pipe(
      tap(data => {
        this.storage.remove("token");
        this.isLoggedIn = false;
        delete this.token;
        return data;
      })
    )
  }

  user(): Observable<User> {
    const headers = new HttpHeaders({
      'Authorization': this.token["token_type"]+" "+this.token["token"]
    });
    return this.http.get<User>(`${this.env.APP_URL}auth/me`, { headers: headers })
    .pipe(
      tap(user => {
        this.userData.next(user);
        return user;
      })
    )
  }

  getToken() {
    return this.storage.getItem('token').then(
      data => {
        this.token = data;
        if(this.token != null) {
          this.isLoggedIn=true;
        } else {
          this.isLoggedIn=false;
        }
      },
      error => {
        this.token = null;
        this.isLoggedIn=false;
      }
    );
  }
}
