import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvService {
  //APP_URL = 'http://staging.abcsystems.com.ph/acts-web-service/public/api/'
  APP_URL = 'http://210.213.91.22:8087/api/'
  //APP_URL = 'http://localhost/acts-web-service/public/api/';

  constructor() { }
}
