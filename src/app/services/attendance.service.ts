import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EnvService } from './env.service';
import { tap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Attendance } from '../models/attendance';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { CurrentLocationService } from './current-location.service';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {
  location: any;

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private autService: AuthService,
    private storage: NativeStorage,
    private locationService: CurrentLocationService
  ) { 

    this.location = {
      latitude: null,
      longitude: null
    }
    
    this.storage.getItem('location').then(data => {
      this.location['latitude'] = data['latitude'];
      this.location['longitude'] = data['longitude'];
    });
  }

  getEmployeeAttendance(employee_no: String){
    const headers = new HttpHeaders({
      'Authorization': this.autService.token["token_type"]+" "+this.autService.token["token"]
    });
    return this.http.get<Attendance>(`${this.env.APP_URL}timelog/verify/`+ employee_no, { headers: headers })
    .pipe(
      tap(attendance => {
        return attendance;
      })
    )
  }

  employeeTimeIn(employee_no: String, attendance_id: BigInteger) {
    const headers = new HttpHeaders({
        'Authorization': this.autService.token["token_type"]+" "+this.autService.token["token"]
      });
      return this.http.post<Attendance>(`${this.env.APP_URL}timelog/store`,
        {
          employee_id: employee_no,
          attendance_id: attendance_id,
          longitude: this.locationService.locationCoords['longitude'],
          latitude: this.locationService.locationCoords['latitude']
        },{ headers: headers })
      .pipe(
        tap(attendance => {
          return attendance;
        })
      )
    }

    employeeTimeOut(employee_no: String, attendance_id: BigInteger) {
        const headers = new HttpHeaders({
            'Authorization': this.autService.token["token_type"]+" "+this.autService.token["token"]
        });
        
        return this.http.post<Attendance>(`${this.env.APP_URL}timelog/time-out`,
        {employee_id: employee_no, attendance_id: attendance_id}, { headers: headers })
        .pipe(
        tap(attendance => {
            return attendance;
        })
        )
    }

    employeeStartBreak(employee_no: String, attendance_id: BigInteger) {
      const headers = new HttpHeaders({
          'Authorization': this.autService.token["token_type"]+" "+this.autService.token["token"]
      });

      return this.http.post<Attendance>(`${this.env.APP_URL}timelog/breaks`, 
        {employee_id: employee_no, attendance_id: attendance_id} , { headers: headers })
      .pipe(
        tap(attendance => {
          return attendance;
        })
      )
    }

    employeeEndBreak(employee_no: String, attendance_id: BigInteger) {
      const headers = new HttpHeaders({
        'Authorization': this.autService.token["token_type"]+" "+this.autService.token["token"]
      });

      return this.http.post<Attendance>(`${this.env.APP_URL}timelog/breaks/time-in`, 
        {employee_id: employee_no, attendance_id: attendance_id} , { headers: headers })
      .pipe(
        tap(attendance => {
          return attendance;
        })
      )
    }
}
